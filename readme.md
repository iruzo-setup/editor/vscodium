# Personal vscodium

- [info](#info)
- [plugins](#plugins)

# info

- Comments in the script got everything you need to set this settings and plugins in Windows/Linux for VSCodium and VSCode

# plugins

| Type          | Extension                         |
|---------------|-----------------------------------|
| Keymap        |                                   |
|               | vscode-eclipse-keybindings        |                                   
| Theme         |                                   |
|               | nord-visual-studio-code           |                        
|               | material-icon-theme               |                    
| Basic         |                                   |
|               | better-comments                   |                
|               | bracket-pair-colorizer-2          |                        
|               | errorlens                         |        
|               | vscode-versionlens                |                    
|               | gitlens                           |        
|               | path-intellisense                 | 
