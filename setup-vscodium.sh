# ----extensions---------------------------------------------------------------

# change "vscodium" for "code" in case you are using VSCode instead VSCodium.
# if you are on windows 10 and using VSCodium, change "vscodium" for "codium"
vscodeVersion=vscodium
extensionInstall=$vscodeVersion\ --install-extension

# theme
$extensionInstall arcticicestudio.nord-visual-studio-code
$extensionInstall PKief.material-icon-theme

# keymap
$extensionInstall alphabotsec.vscode-eclipse-keybindings

# basic
$extensionInstall aaron-bond.better-comments
$extensionInstall CoenraadS.bracket-pair-colorizer-2
$extensionInstall christian-kohler.path-intellisense
$extensionInstall usernamehw.errorlens
$extensionInstall pflannery.vscode-versionlens
$extensionInstall eamodio.gitlens

# ----settings-----------------------------------------------------------------

# If you are in windows, you can run this script from Git Bash terminal and change the userVSCodiumPath line:
# userVSCodiumPath=$HOME/AppData/Roaming/Code/User

# e.g. of default user VSCode settings path in Git Bash on Windows: /c/Users/User/AppData/Roaming/Code/User

# If you are using VSCode instead of VSCodium, change "VSCodium" for "Code"
userVSCodiumPath=$HOME/.config/VSCodium/User

rm $userVSCodiumPath/settings.json

echo '{' >> $userVSCodiumPath/settings.json
echo '    "window.menuBarVisibility": "toggle",' >> $userVSCodiumPath/settings.json
echo '    "workbench.statusBar.visible": false,' >> $userVSCodiumPath/settings.json
echo '    "workbench.activityBar.visible": false,' >> $userVSCodiumPath/settings.json
echo '    "workbench.iconTheme": "material-icon-theme",' >> $userVSCodiumPath/settings.json
echo '    "terminal.integrated.fontFamily": "monospace",' >> $userVSCodiumPath/settings.json
echo '    "editor.fontLigatures": true,' >> $userVSCodiumPath/settings.json
echo '    "explorer.confirmDelete": false,' >> $userVSCodiumPath/settings.json
echo '    "workbench.colorTheme": "Nord",' >> $userVSCodiumPath/settings.json
echo '    "editor.minimap.enabled": false' >> $userVSCodiumPath/settings.json
echo '}' >> $userVSCodiumPath/settings.json
